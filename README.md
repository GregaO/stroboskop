# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://GregaO@bitbucket.org/GregaO/stroboskop.git

Naloga 6.2.3:

https://bitbucket.org/GregaO/stroboskop/commits/26943428ae849d411f92a0e5fff607b0e1ec5625

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

https://bitbucket.org/GregaO/stroboskop/commits/31cc944464217122d202c6eaae80ecbd77259b74

Naloga 6.3.2:

https://bitbucket.org/GregaO/stroboskop/commits/2876569f294d69932b451f1bd78f5ccc4bec65a8

Naloga 6.3.3:

https://bitbucket.org/GregaO/stroboskop/commits/595847b6b3d97cf4201e9e538f1a261ee072604b

Naloga 6.3.4:

https://bitbucket.org/GregaO/stroboskop/commits/385f572d7b81fafad40e7c91a2475e95fad4b606

Naloga 6.3.5:

git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

https://bitbucket.org/GregaO/stroboskop/commits/a343061e4b8b7f3a5aebdba94fcaef6dcabf0708

Naloga 6.4.2:

https://bitbucket.org/GregaO/stroboskop/commits/a765299516bbf22bc588b6bafa04f899e3846801

Naloga 6.4.3:

https://bitbucket.org/GregaO/stroboskop/commits/06293a2c2126a84c63c10f701c1be3f43e350f18

Naloga 6.4.4:

https://bitbucket.org/GregaO/stroboskop/commits/0c10bb750438fdbb9cb934959392e05f4f8946af

/